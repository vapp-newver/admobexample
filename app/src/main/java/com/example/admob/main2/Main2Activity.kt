package com.example.admob.main2

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.example.admob.EmptyActivity1
import com.example.admob.EmptyActivity2
import com.example.admob.LifecycleHelper
import com.example.admob.R
import com.vapp.admoblibrary.AdmodUtils
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity(),LifecycleObserver {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        AdmodUtils.loadAdNativeAds(this, AdmodUtils.ads_admob_native_test_id, my_template)
        btn.setOnClickListener {
            AdmodUtils.loadAndShowAdReward(this, AdmodUtils.ads_admob_rewarded_test_id)
        }
        this.lifecycle.addObserver(this)
        btnNext.setOnClickListener {
            startActivity(Intent(this@Main2Activity, EmptyActivity1::class.java))
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onCreateActivity() {
        AdmodUtils.isAdShowing = true
    }
}