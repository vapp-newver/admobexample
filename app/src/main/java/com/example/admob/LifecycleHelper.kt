package com.example.admob

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.vapp.admoblibrary.AdmodUtils

object LifecycleHelper: LifecycleObserver {
    fun registerLifecycle(lifecycle : Lifecycle){
        lifecycle.addObserver(this)
    }
    /** LifecycleObserver methods  */
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreateActivity() {
        AdmodUtils.isAdShowing = false
    }
}